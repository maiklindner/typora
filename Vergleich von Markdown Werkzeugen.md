# Vergleich von Markdown Werkzeugen

| Tool       | Gliederungsanzeige | Instant Preview | Export    |
| ---------- | ------------------ | --------------- | --------- |
| Typora     | x                  | x               | pdf, html |
| Moeditor   | -                  | x               | pdf, html |
| Abricotine | -                  | -               | html      |


